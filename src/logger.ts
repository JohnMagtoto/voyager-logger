import * as moment from 'moment';

/**
 * Logging levels recognized by logger.
 * INFO : For information only logging.
 * WARN : For warning messages
 * ERROR : For error messages
 */
export enum LOG_LEVELS {
    LOG = 0,
    INFO = 1,
    DEBUG = 2,
    WARN = 3,
    ERROR = 4
}

/**
 * The core logging class
 * Created for the purpose of controlled logging to window console (or derivatives)
 */
export class Logger {
    // The specified logging levels of core
    private _currentLogLevel: LOG_LEVELS;

    private readonly LOG_STRING = 'LOG';

    private readonly INFO_STRING = 'INFO';

    private readonly DEBUG_STRING = 'DEBUG';

    private readonly WARN_STRING = 'WARN';

    private readonly ERROR_STRING = 'ERROR';

    private readonly VOYAGER_ERROR_STRING = 'VOYAGER ERROR';

    /**
     * Constructor for Logger class
     * @param _level The logging level to use
     * NOTE: Remote POST is being reworked!
     */
    constructor(private _level: LOG_LEVELS) {
        this._currentLogLevel = _level;
    }

    /**
     * Execute Logging
     * @param level The explicit stated level of the log entry
     * @param message The message payload
     * @param payload Any extra data payload
     */
    public logMessage(level: LOG_LEVELS, message?: string, payload?: any) {
        // Identify the logging needed
        switch (level) {
            case LOG_LEVELS.LOG:
                this.log(message, payload);
                break;
            case LOG_LEVELS.INFO:
                this.info(message, payload);
                break;
            case LOG_LEVELS.DEBUG:
                this.debug(message, payload);
                break;
            case LOG_LEVELS.WARN:
                this.warn(message, payload);
                break;
            case LOG_LEVELS.ERROR:
                this.error(message, payload);
                break;
            default:
                // Do not do anything if passed level does not match any of the log levels
                this._voyagerError('The passed log level does not exist!');
        }
    }

    /**
     * Simple message log
     * @param message The message payload
     * @param payload Any extra data payload
     */
    public log(message?: string, payload?: any) {
        // Do not do anything if it is less than the current log level
        if (this._currentLogLevel > LOG_LEVELS.LOG) {
            return;
        }
        // Display the message in console
        this._logMessage(this.LOG_STRING, message, payload);
    }

    /**
     * Info log
     * @param message The message payload
     * @param payload Any extra data payload
     */
    public info(message?: string, payload?: any) {
        // Do not do anything if it is less than the current log level
        if (this._currentLogLevel > LOG_LEVELS.INFO) {
            return;
        }
        // Display the message in console
        this._logMessage(this.INFO_STRING, message, payload);
    }

    /**
     * Debug log
     * @param message The message payload
     * @param payload Any extra data payload
     */
    public debug(message?: string, payload?: any) {
        // Do not do anything if it is less than the current log level
        if (this._currentLogLevel > LOG_LEVELS.DEBUG) {
            return;
        }
        // Display the message in console
        this._logMessage(this.DEBUG_STRING, message, payload);
    }

    /**
     * Warning log
     * @param message The message payload
     * @param payload Any extra data payload
     */
    public warn(message?: string, payload?: any) {
        // Do not do anything if it is less than the current log level
        if (this._currentLogLevel > LOG_LEVELS.WARN) {
            return;
        }
        // Display the message in console
        this._logMessage(this.WARN_STRING, message, payload);
    }

    /**
     * Error log
     * @param message The message payload
     * @param payload Any extra data payload
     */
    public error(message?: string, payload?: any) {
        // Do not do anything if it is less than the current log level
        if (this._currentLogLevel > LOG_LEVELS.ERROR) {
            return;
        }
        // Display the message in console
        this._logMessage(this.ERROR_STRING, message, payload);
    }

    private _voyagerError(message?: string, payload?: any) {
        this._logMessage(this.VOYAGER_ERROR_STRING, message, payload);
    }

    /**
     * Log the message in console
     * @param levelAsString The logging level as string
     * @param message The message string
     * @param payload Any extra data payload
     */
    private _logMessage(
        levelAsString: string,
        message?: string,
        payload?: string
    ) {
        if (message === undefined && payload === undefined) {
            this._logMessage(
                this.WARN_STRING,
                null,
                'no message or payload found'
            );
        }

        // Create the timestamp for the log
        const timeStamp = moment().toISOString();

        // Set parameters to empty string in case of undefined
        message = message === undefined ? '' : message;
        payload = payload === undefined ? '' : payload;

        switch (levelAsString) {
            case this.LOG_STRING:
                console.log(
                    timeStamp.concat(
                        ' - ',
                        levelAsString,
                        ' : ',

                        message
                    ),
                    payload
                );
                break;
            case this.INFO_STRING:
                console.info(
                    timeStamp.concat(
                        ' - ',
                        levelAsString,
                        ' : ',

                        message
                    ),
                    payload
                );
                break;
            case this.DEBUG_STRING:
                console.debug(
                    timeStamp.concat(
                        ' - ',
                        levelAsString,
                        ' : ',

                        message
                    ),
                    payload
                );
                break;
            case this.WARN_STRING:
                console.warn(
                    timeStamp.concat(
                        ' - ',
                        levelAsString,
                        ' : ',

                        message
                    ),
                    payload
                );
                break;
            case this.ERROR_STRING:
                console.error(
                    timeStamp.concat(
                        ' - ',
                        levelAsString,
                        ' : ',

                        message
                    ),
                    payload
                );
                break;
            case this.VOYAGER_ERROR_STRING:
                console.error(levelAsString, ' : '.concat(message), payload);
            default:
                // Do not do anything if passed level does not match any of the log levels
                this.error('The passed log level does not exist!');
        }
    }
}
