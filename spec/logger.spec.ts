import { Logger, LOG_LEVELS } from './../src/logger';

describe('Voyager Logger test', () => {
    it('should run test', () => {
        expect(true).toBeTruthy();
    });

    it('should be able to call log method', () => {
        // spy on console log
        spyOn(console, 'log');
        // setup logger to show log type prints
        const logger = new Logger(LOG_LEVELS.LOG);

        // actual test
        logger.log('sample log');
        expect(console.log).toHaveBeenCalled();
        expect(console.log).toHaveBeenCalledTimes(1);
    });

    it('should be able to call log via log message', () => {
        // spy on console log
        spyOn(console, 'log');
        // setup logger to show log type prints
        const logger = new Logger(LOG_LEVELS.LOG);

        // actual test
        logger.logMessage(LOG_LEVELS.LOG, 'sample log');
        expect(console.log).toHaveBeenCalled();
        expect(console.log).toHaveBeenCalledTimes(1);
    });

    it('should be able to call info method', () => {
        // spy on console info
        spyOn(console, 'info');
        // setup logger to show info type prints
        const logger = new Logger(LOG_LEVELS.INFO);

        // actual test
        logger.info('sample info');
        expect(console.info).toHaveBeenCalled();
        expect(console.info).toHaveBeenCalledTimes(1);
    });

    it('should be able to call info via log message', () => {
        // spy on console info
        spyOn(console, 'info');
        // setup logger to show info type prints
        const logger = new Logger(LOG_LEVELS.INFO);

        // actual test
        logger.logMessage(LOG_LEVELS.INFO, 'sample info');
        expect(console.info).toHaveBeenCalled();
        expect(console.info).toHaveBeenCalledTimes(1);
    });

    it('should be able to call debug method', () => {
        // spy on console info
        spyOn(console, 'debug');
        // setup logger to show debug type prints
        const logger = new Logger(LOG_LEVELS.DEBUG);

        // actual test
        logger.debug('sample debug');
        expect(console.debug).toHaveBeenCalled();
        expect(console.debug).toHaveBeenCalledTimes(1);
    });

    it('should be able to call debug via log message', () => {
        // spy on console info
        spyOn(console, 'debug');
        // setup logger to show debug type prints
        const logger = new Logger(LOG_LEVELS.DEBUG);

        // actual test
        logger.logMessage(LOG_LEVELS.DEBUG, 'sample debug');
        expect(console.debug).toHaveBeenCalled();
        expect(console.debug).toHaveBeenCalledTimes(1);
    });

    it('should be able to call warn method', () => {
        // spy on console warn
        spyOn(console, 'warn');
        // setup logger to show warn type prints
        const logger = new Logger(LOG_LEVELS.WARN);

        // actual test
        logger.warn('sample warn');
        expect(console.warn).toHaveBeenCalled();
        expect(console.warn).toHaveBeenCalledTimes(1);
    });

    it('should be able to call warn via log message', () => {
        // spy on console warn
        spyOn(console, 'warn');
        // setup logger to show warn type prints
        const logger = new Logger(LOG_LEVELS.WARN);

        // actual test
        logger.logMessage(LOG_LEVELS.WARN, 'sample warn');
        expect(console.warn).toHaveBeenCalled();
        expect(console.warn).toHaveBeenCalledTimes(1);
    });

    it('should be able to call error method', () => {
        // spy on console error
        spyOn(console, 'error');
        // setup logger to show error type prints
        const logger = new Logger(LOG_LEVELS.ERROR);

        // actual test
        logger.error('sample error');
        expect(console.error).toHaveBeenCalled();
        expect(console.error).toHaveBeenCalledTimes(1);
    });

    it('should be able to call error via log message', () => {
        // spy on console error
        spyOn(console, 'error');
        // setup logger to show error type prints
        const logger = new Logger(LOG_LEVELS.ERROR);

        // actual test
        logger.logMessage(LOG_LEVELS.ERROR, 'sample error');
        expect(console.error).toHaveBeenCalled();
        expect(console.error).toHaveBeenCalledTimes(1);
    });

    it('should be not display message lower than set log level', () => {
        // spy on console log
        spyOn(console, 'log');
        // setup logger to show error type prints
        const logger = new Logger(LOG_LEVELS.ERROR);

        // actual test
        logger.log('sample error');
        expect(console.log).not.toHaveBeenCalled();
        expect(console.log).not.toHaveBeenCalledTimes(1);
    });

    // TODO: Re-implement POST Tests when feature is re-implemented
});
